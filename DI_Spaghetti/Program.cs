using DI_Spaghetti;
using DI_Spaghetti.Services;

using static DI_Spaghetti.Services.ConnectionStringsManager;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();


//builder.Services.AddScoped<IDapperContext, DapperContext>();
builder.Services.AddSingleton<ITenantInformation, TenantInformation>();
builder.Services.AddScoped<IContextUserService, ContextUserService>();
builder.Services.AddSingleton<IConnectionStringsManager, ConnectionStringsManager>();
builder.Services.AddScoped<IUnitOfWork, UnitOfWork>();
builder.Services.AddSingleton<ISingletonB, Singleton>();

builder.Services.AddSingleton<IContextFactory, ContextFactory>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.UseMiddleware<Middleware>();

app.MapControllers();

app.Run();
