﻿using static DI_Spaghetti.Services.ConnectionStringsManager;

namespace DI_Spaghetti.Services
{

    public interface IUnitOfWork
    {        
        IRepositoryA RepositoryA { get; }

        IRepositoryB RepositoryB { get; }

        //IRepositoryC RepositoryC { get; }

        //IRepositoryD RepositoryD { get; }
    }

    public class UnitOfWork: IUnitOfWork
    {
        private readonly Lazy<IRepositoryA> _repositoryA;
        private readonly Lazy<IRepositoryB> _repositoryB;
        //private readonly Lazy<IRepositoryC> _repositoryC;
        //private readonly Lazy<IRepositoryD> _repositoryD;

        //private readonly DapperContext _context;


       // [DatabaseAccessScope(DBAccessScope.Iris)]
        public IRepositoryA RepositoryA => _repositoryA.Value;


       // [DatabaseAccessScope(DBAccessScope.SingleOperator)]
        public IRepositoryB RepositoryB => _repositoryB.Value;

        /*

        [DatabaseAccessScope(DBAccessScope.AllOperators)]
        public IRepositoryC RepositoryC => _repositoryC.Value;


        [DatabaseAccessScope(DBAccessScope.All)]
        public IRepositoryD RepositoryD => _repositoryD.Value;
        */
        //public IDapperContext DapperContext => _context;

        public UnitOfWork(IContextFactory contextFactory)
        {
            _repositoryA = new Lazy<IRepositoryA>(() => new RepositoryA<BaseContext>(contextFactory));
            _repositoryB = new Lazy<IRepositoryB>(() => new RepositoryB<OperatorContext>(contextFactory));
            //_repositoryC = new Lazy<IRepositoryC>(() => new RepositoryC(context));
            //_repositoryD = new Lazy<IRepositoryD>(() => new RepositoryD(context));
        }
    }
}
