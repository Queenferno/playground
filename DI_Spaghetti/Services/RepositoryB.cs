﻿using System.Reflection;

using static DI_Spaghetti.Services.ConnectionStringsManager;

namespace DI_Spaghetti.Services
{
    public interface IRepositoryB
    {
        string Name { get; }

        void Run();
    }



    public class RepositoryB<IContext> : GenericRepository<IContext>, IRepositoryB where IContext : IDapperContext
    {
        public string Name => "Repository B";

        public RepositoryB(IContextFactory contextFactory) : base(contextFactory)
        {

        }

        public void Run()
        {
            Console.WriteLine($"{Name} is Running");
            Context.Save();
        }
    }
}
