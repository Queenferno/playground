﻿using System.Reflection;

using static DI_Spaghetti.Services.ConnectionStringsManager;

namespace DI_Spaghetti.Services
{
    public class GenericRepository<TContext> where TContext: IDapperContext
    {
        protected readonly bool UseOperatorDB;

        protected IDapperContext Context;

        public GenericRepository(IContextFactory contextFactory)
        {
            Context = contextFactory.SetContext(typeof(TContext));
        }

        public void Save()
        {
            Context.Save();
        }       
    }
}
