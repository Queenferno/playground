﻿using System.Reflection;

using static DI_Spaghetti.Services.ConnectionStringsManager;

namespace DI_Spaghetti.Services
{
    public interface IRepositoryA
    {
        string Name { get; }

        void Run();
    }



    public class RepositoryA<TContext> : GenericRepository<TContext>, IRepositoryA where TContext : IDapperContext
    {

        public string Name => "Repository A";

        public RepositoryA(IContextFactory contextFactory) :base(contextFactory)
        {

        }

        public void Run()
        {
            Console.WriteLine($"{Name} is Running");
            Save();
        }
    }
}
