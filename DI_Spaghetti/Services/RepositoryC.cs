﻿using System.Reflection;

namespace DI_Spaghetti.Services
{
    /*
    public interface IRepositoryC
    {
        string Name { get; }

        void Run();
    }



    public class RepositoryC : GenericRepository, IRepositoryC
    {
        private readonly DapperContext _context;
        public string Name => "Repository A";

        public RepositoryC(DapperContext context):base(context, typeof(RepositoryA))
        {
            _context = context;
        }

        public void Run()
        {
            Console.WriteLine($"{Name} is Running");
            _context.Save();
        }
    }
    */
}
