﻿namespace DI_Spaghetti.Services
{
    public static class Helpers
    {
        public static int GetID(int max = int.MaxValue)
        {
            Random random = new Random();
            return random.Next(1, max);
        }
    }
}
