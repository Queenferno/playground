﻿namespace DI_Spaghetti.Services
{
    public interface ITenantInformation
    {
        void SetOperator(int? id);
        int? GetOperator();
    }

    public class TenantInformation : ITenantInformation
    {
        private int? _operatorId;

        public int? GetOperator()
        {
            return _operatorId;
        }

        public void SetOperator(int? operatorId)
        {
            _operatorId = operatorId;
        }
    }
}
