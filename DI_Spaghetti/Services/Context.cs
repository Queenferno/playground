﻿namespace DI_Spaghetti.Services
{
    public interface IDapperContext
    {
        void Save();
    }

    public class OperatorContext : BaseContext
    {
        //protected override string _connectionString { get; set; }
        public OperatorContext(string connectionString) : base(connectionString)
        {
            //_connectionString = _connectionStringsManager.GetConnectionString();
            SetObjectId(typeof(OperatorContext));
        }
    }

    public class MultiOperatorContext : BaseContext
    {
        //protected override List<string> _connectionStrings { get; set; } = new();

        public MultiOperatorContext(List<string> connectionStrings) : base(connectionStrings)
        {

            SetObjectId(typeof(MultiOperatorContext));
        }
    }

    public class AllDBsContext : BaseContext
    {
        //private List<string> _connectionStrings { get; set; }

        public AllDBsContext(List<string> connectionStrings) : base(connectionStrings)
        {
            SetObjectId(typeof(AllDBsContext));
        }
    }

    /// <summary>
    /// Iris Users DB and common functionality
    /// </summary>
    public class BaseContext : ObjectIdentifier, IDapperContext
    {
        protected virtual string _connectionString { get; set; }
        protected virtual List<string> _connectionStrings { get; set; }


        public BaseContext(string connectionString)
        {
            _connectionString = connectionString;
            SetObjectId(typeof(BaseContext));
        }

        public BaseContext(List<string> connectionStrings)
        {
            _connectionStrings = connectionStrings;
            SetObjectId(typeof(BaseContext));
        }

        public virtual void Save()
        {
            LogCallerFunction(typeof(BaseContext), $"Save()");
            Console.WriteLine($"Saving with ConnString [{_connectionString}]");
        }
    }
}
