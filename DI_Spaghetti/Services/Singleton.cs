﻿namespace DI_Spaghetti.Services
{
    public interface ISingletonB
    {
        void RunInMultiDBs();
        void Run();
    }
    public class Singleton: ISingletonB
    {
        private readonly IServiceScopeFactory _scope;
        public Singleton(IServiceScopeFactory scope)
        {
            _scope = scope;
        }

        public void Run()
        {
            Console.WriteLine("Singleton running");

            using var scope = _scope.CreateScope();

            var uof = scope.ServiceProvider.GetRequiredService<IUnitOfWork>();

            uof.RepositoryA.Run();
            
            uof.RepositoryB.Run();
        }

        public void RunInMultiDBs()
        {
            using var scope = _scope.CreateScope();

            var uof = scope.ServiceProvider.GetRequiredService<IUnitOfWork>();

            Console.WriteLine("---------------------------------------------------------");
            Console.WriteLine("1. Calling RepoA: Must run only on Iris");
            uof.RepositoryA.Run();

            Console.WriteLine("\n---------------------------------------------------------");
            Console.WriteLine("2. Calling RepoB: Must run only on Op");
            uof.RepositoryB.Run();

            Console.WriteLine("\n---------------------------------------------------------");
            Console.WriteLine("3. Calling RepoA: Must run only on Iris");
            uof.RepositoryA.Run();

            Console.WriteLine("\n---------------------------------------------------------");
            Console.WriteLine("4. Calling RepoB: Must run only on Op");
            uof.RepositoryB.Run();

            Console.WriteLine("---------------------------------------------------------");
            Console.WriteLine("5. Calling RepoA: Must run only on Iris");
            uof.RepositoryA.Run();

            Console.WriteLine("\n---------------------------------------------------------");
            Console.WriteLine("6. Calling RepoB: Must run only on Op");
            uof.RepositoryB.Run();
        }
    }
}
