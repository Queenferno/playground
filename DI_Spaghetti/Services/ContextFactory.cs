﻿namespace DI_Spaghetti.Services
{


    public interface IContextFactory//<TContext> where TContext : IDapperContext
    {
        //IDapperContext Context { get; }
        IDapperContext SetContext(Type forContext);
    }

    public class ContextFactory : IContextFactory
    {
        private readonly IConnectionStringsManager _connectionStringManager;


        private IDictionary<Type, IDapperContext> _contexts = new Dictionary<Type, IDapperContext>();

        public ContextFactory(IConnectionStringsManager connectionStringManager)
        {
            Console.WriteLine($"========================== new ContextFactory() =======================================");

            _connectionStringManager = connectionStringManager;
        }

        public IDapperContext SetContext(Type forContext)
        {
            Console.WriteLine($"ContextFactory.SetContext({forContext.Name})");
            return BuildContext(forContext);
        }

        private IDapperContext BuildContext(Type forContext)
        {
            Console.WriteLine($"ContextFactory.BuildContext({forContext.Name})");

            if(_contexts.ContainsKey(forContext))
            {
                Console.WriteLine("Getting Context from Dictionary");
                return _contexts[forContext];
            }

            IDapperContext context = null;
            Console.WriteLine("Resolving Context from scratch");

            if (forContext == typeof(BaseContext))
            {
                context = new BaseContext(_connectionStringManager.GetDefaultConnectionString());
            }

            if (forContext == typeof(OperatorContext))
            {
                context = new OperatorContext(_connectionStringManager.GetOperatorConnectionString());
            }

            _contexts.Add(forContext, context!);
            return context;
            throw new NotImplementedException();
        }
    }
}
