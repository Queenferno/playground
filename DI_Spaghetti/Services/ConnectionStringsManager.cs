﻿using System;

namespace DI_Spaghetti.Services
{
    public interface IConnectionStringsManager
    {
        string GetDefaultConnectionString();
        string GetOperatorConnectionString();
        List<string> GetOperatorConnectionStrings();
        List<string> GetAllConnectionStrings();
    }

    public class ConnectionStringsManager: IConnectionStringsManager
    {
        private readonly ITenantInformation _contextUserService;
        public ConnectionStringsManager(ITenantInformation contextUserService)
        {
            _contextUserService = contextUserService;
        }

        public string GetDefaultConnectionString()
        {
            return "IrisUsers_ConnectionString";
        }

        public string GetOperatorConnectionString()
        {
            string result = "";

            var defaultConnString = GetDefaultConnectionString();

            var opId = _contextUserService.GetOperator();
            if (!opId.HasValue) return defaultConnString;
            result = $"[{opId.Value}_ConnectionString]";

            Console.WriteLine($"ConnectionStringsManager.GetConnectionString() returns {result}");
            return result;
        }

        public List<string> GetOperatorConnectionStrings()
        {
            throw new NotImplementedException();
        }

        public List<string> GetAllConnectionStrings()
        {
            throw new NotImplementedException();
        }
    }
}
