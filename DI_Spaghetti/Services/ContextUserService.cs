﻿namespace DI_Spaghetti.Services
{
    public class ContextUser
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public int? SelectedOperator { get; set; }
    }
    public interface IContextUserService
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        void SetUser(ContextUser? user);

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        int GetUserId();

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        string GetUsername();

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        bool IsValidSelectedOperator();

    }

    public class ContextUserService : IContextUserService, ITenantInformation
    {
        private readonly ITenantInformation _tenantInformation;

        public ContextUserService(ITenantInformation tenantInformation)
        {
            _tenantInformation = tenantInformation;
        }
        /// <summary>
        /// A thinner version of the HttpContext.User, for internal use
        /// </summary>
        private ContextUser? _user { get; set; }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException">Thrown if User is null</exception>
        public int GetUserId()
        {
            ValidateUser();

            return _user!.Id;
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException">Thrown if User is null</exception>
        public string GetUsername()
        {
            ValidateUser();

            return _user!.UserName;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        public void SetUser(ContextUser? user)
        {
            _user = user;
            SetOperator(_user?.SelectedOperator);
        }

        /// <summary>
        ///
        /// </summary>
        /// <exception cref="InvalidOperationException">Thrown if User is null</exception>
        private void ValidateUser()
        {
            if (_user == null)
            {
                throw new InvalidOperationException("Null user");
            }
        }

        public bool IsValidSelectedOperator()
        {
            throw new NotImplementedException();
        }

        public void SetOperator(int? id)
        {
            _tenantInformation.SetOperator(id);
        }

        public int? GetOperator()
        {
            return _tenantInformation.GetOperator();
        }
    }
}
