﻿namespace DI_Spaghetti.Services
{
    public abstract class ObjectIdentifier
    {
        public int ObjectId { get; set; }

        public void SetObjectId(Type caller)
        {
            ObjectId = Helpers.GetID(100);

            LogCallerFunction(caller, "new()");
        }

        public string GetObjName(Type caller)
        {
            return $"[{caller.Name} : {ObjectId}] - ";
        }

        public void LogCallerFunction(Type caller, string func)
        {
            Console.WriteLine($"{GetObjName(caller)}{func}");
        }
    }
}
