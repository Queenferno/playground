using DI_Spaghetti.Services;

using Microsoft.AspNetCore.Mvc;

namespace DI_Spaghetti.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private readonly ISingletonB _service; 

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ISingletonB service, ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
            _service = service;
        }

        [HttpGet]
        [Route("actionA")]
        public ActionResult ActionA()
        {
            _service.Run();

            return Ok();
        }

        [HttpGet]
        [Route("actionB")]
        public ActionResult ActionB()
        {
            _service.Run();

            return Ok();
        }

        [HttpGet]
        [Route("actionC")]
        public ActionResult ActionC()
        {
            _service.RunInMultiDBs();

            return Ok();
        }
    }
}