﻿using DI_Spaghetti.Services;

namespace DI_Spaghetti
{
    public class Middleware
    {
        private readonly RequestDelegate _next;

        public Middleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            var service = context.RequestServices.GetRequiredService<IContextUserService>();

            service.SetUser(new ContextUser { SelectedOperator = Helpers.GetID(21) });

            await _next(context);
        }
    }
}
